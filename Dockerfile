ARG TAG
FROM debian:${TAG}
RUN \
  apt-get update -q && \
  apt-get install -yq apt-transport-https ca-certificates wget gnupg2 curl unzip jq bzip2 && \
  apt-get clean -yq
RUN \
  groupadd --gid 3000 runtime && \
  useradd --gid 3000 --uid 1000 runtime && \
  mkdir -p /home/runtime && \
  chown 1000:3000 /home/runtime
